import React, { Fragment} from 'react';
import Counter from './../containers/CounterContainer';

function Home() {

  return (
    <Fragment>
      <Counter />
    </Fragment>
  );
};

export default Home;
