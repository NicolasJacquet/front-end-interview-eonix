import React from 'react';

function Counter({
  onIncrement,
  onDecrement,
  counter,
}) {

  const handleIncrementClick = () => {
    onIncrement();
  }
  
  const handleDecrementClick = () => {
    onDecrement();
  }

  const isDecrementActive = counter < 1;

  return (
    <div>
      <div>
        {counter}
      </div>
      <button onClick={handleIncrementClick}>
        Incrémenter le compteur
      </button>
      <button onClick={handleDecrementClick} disabled={isDecrementActive}>
        Décrémenter le compteur
      </button>
    </div>
  );
};

export default Counter;
